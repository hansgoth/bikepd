# README #

Quick installation guide

### Description ###

* Bike Police Department System
* 1.0.0

### Installation ###

* Requirements: node.js installed
* Copy template.env to .env
* Environment is already configured, no need to adjust anything ;)
* Online Database is already initialized also.
* "npm install" to get all dependencies
* "npm start"

