// Imports
const dotenv            = require('dotenv');
const express           = require('express');
const fileUpload        = require('express-fileupload');
const cookieParser      = require('cookie-parser');
const cron              = require('node-cron');
const cors              = require('cors');
const path              = require('path');
const browser           = require('./middleware/browser');


// Globals
const server = express();


// Settings
dotenv.config();
server.set('views', path.join(__dirname, 'views'));
server.set('view engine', 'ejs');
server.use(cors());
server.use(fileUpload({ createParentPath: true}));
server.use(express.json());
server.use(express.urlencoded({ extended: true }));
server.use(cookieParser());
server.use(browser.serveBrowser);

// Config routes
server.use('/api/v1', require('./routes/api'));
server.use('/'      , require('./routes/web'));


// Start Server
server.listen(process.env.PORT, ()=>{
    console.log(`Server started at port ${process.env.PORT}`);
});


//TODO generic error capture

//Cron Job Started
var task = cron.schedule(process.env.JOB_CRON, require('./controllers/reportController').assignReports, cron.Schedul);
