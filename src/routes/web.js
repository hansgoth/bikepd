const { Router } = require('express');
const jwt        = require('../middleware/jwt');
const webController = require('../controllers/webController');


const router = new Router;

router.get('/', jwt.verifyToken, webController.showDashboard);

router.get('/login', jwt.verifyNoToken, webController.showLogin);

router.get('/register',jwt.verifyNoToken, webController.showRegister);

router.get('/favicon.ico', webController.getFavicon);

router.get('*', jwt.verifyToken, webController.showDashboard);


module.exports = router;