const { Router }           = require('express');
const database             = require('../database/db');
const package              = require('../../package.json');
const userController       = require('../controllers/userController');
const reportController     = require('../controllers/reportController');
const bikeController       = require('../controllers/bikeController');
const officerController    = require('../controllers/officerController');
const departmentController = require('../controllers/departmentController');
const jwt                  = require('../middleware/jwt');
const roles                = require('../middleware/roles');

const router = new Router;

// API Info
router.get('/', (req, res, next)=>{
    res.status(200).json({
        name: package.name,
        author: package.author,
        description: package.description,
        version: package.version,
    });
});


//Users
router.post('/register', userController.registerUser );
router.post('/login', userController.loginUser );
router.get ('/logout', jwt.verifyToken, userController.logOut );

//TODO research: router.use(jwt.verifyToken);
router.post('/report', jwt.verifyToken, reportController.createReport );
router.get ('/report', jwt.verifyToken, roles.isOfficer ,reportController.getReports );
router.post('/report/id', jwt.verifyToken, reportController.getReport );
router.get ('/report/:id',jwt.verifyToken, reportController.getReport );
router.get ('/report/:id/close',jwt.verifyToken, roles.isOfficer, reportController.closeReport );

//Officers
router.post('/search', jwt.verifyToken, roles.isOfficer, bikeController.searchBikes );

//Director
router.post('/officer', jwt.verifyToken, roles.isDirector, officerController.addOfficer);

//SuperAdmin
router.post('/department', jwt.verifyToken, roles.isSuperAdmin, departmentController.addDepartment );



module.exports = router;