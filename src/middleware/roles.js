const database   = require('../database/db');

const ROLE_USER       = 'user';
const ROLE_OFFICER    = 'officer';
const ROLE_DIRECTOR   = 'director';
const ROLE_SUPERADMIN = 'superadmin';


// Verifies User Role
async function isUser(req, res, next){
    try {

        if( await isRole(req.user, ROLE_USER) )
            next();
        else
            return res.status(401).json( {message: 'Not a valid User'} );
        
    } catch (error) {
        //Token expired or jwt error
        return res.status(401).json( {message: 'User verification failed', error: error} );
    }
}


// Verifies Officer Role
async function isOfficer(req, res, next){
    try {

        if( await isRole(req.user, ROLE_OFFICER) ){
            getOfficer(req);
            next();
        }
        else
            return res.status(401).json( {message: 'Not a valid officer'} );
        
    } catch (error) {
        //Token expired or jwt error
        return res.status(401).json( {message: 'Officer verification failed', error: error} );
    }
}


// Verifies Director Role
async function isDirector(req, res, next){
    try {

        if( await isRole(req.user, ROLE_DIRECTOR) ){
            getOfficer(req);
            next();
        }
        else
            return res.status(401).json( {message: 'Not a valid Director'} );
        
    } catch (error) {
        //Token expired or jwt error
        return res.status(401).json( {message: 'Director verification failed', error: error} );
    }
}


// Verifies SuperAdmin Role
async function isSuperAdmin(req, res, next){
    try {

        if( await isRole(req.user, ROLE_SUPERADMIN) )
            next();
        else
            return res.status(401).json( {message: 'Not a valid SuperAdmin'} );
        
    } catch (error) {
        //Token expired or jwt error
        return res.clearCookie('token').status(401).json( {message: 'SuperAdmin verification failed', error: error} );
    }
}


async function isRole(user, role){
    //resolve list of roles
    if(user.roles === undefined ){
        let roles = await database.query(`SELECT public.roles.name FROM public.users_roles INNER JOIN public.roles ON public.users_roles.role_id = public.roles.id WHERE public.users_roles.user_id = ${user.id}`)
        user.roles = roles.rows.map( role=>role.name);
    }
    return user.roles.includes(role);
}

async function getOfficer(req){
    let officer = await database.query(`SELECT public.officers.*, public.departments.name AS dep_name FROM public.officers INNER JOIN public.departments ON public.departments.id = public.officers.department_id WHERE public.officers.deleted_at IS NULL AND public.officers.user_id = ${req.user.id}`);
    req.officer = officer.rows[0];
}

module.exports = { ROLE_USER, ROLE_OFFICER, ROLE_DIRECTOR, ROLE_SUPERADMIN, isUser, isOfficer, isDirector, isSuperAdmin, isRole, getOfficer }