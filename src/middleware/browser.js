// Check user-agent header to determine if request comes from browsers
function serveBrowser(req, res, next){
    try {
        let userAgent = req.headers["user-agent"];
        req.serveBrowser = ( typeof userAgent !== 'undefined' && userAgent !== undefined && userAgent !== null && userAgent.trim() != '' );
    } catch (error) {
        console.error('Unexpected Excception checkin serverBrowser Middleware');
    }
    next();
}

module.exports = { serveBrowser }