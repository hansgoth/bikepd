const jwt        = require('jsonwebtoken');
const database   = require('../database/db');

// Sets the token on the responce
function setToken(id, res){
    let token = jwt.sign( {id: id}, process.env.JWT_SECRET, {
        expiresIn: parseInt(process.env.JWT_TIMEOUT,10)
    });
    res.cookie('token',token);
}


// Verifies token and valid user
async function verifyToken(req, res, next){
    try {

        let decoded = jwt.verify(req.cookies.token, process.env.JWT_SECRET);
        let query = await database.query(`SELECT * FROM public.users WHERE deleted_at IS NULL AND id = '${decoded.id}';`);
        if(query.rowCount==1) {
            res.cookie('token',req.cookies.token); //set cookie for next res
            req.user = query.rows[0]; // sets user on req for convinience
            next();
        }
        else {
            //Unexpected token user disapeared (token manipulation)
            if(req.serveBrowser)
                return res.clearCookie('token').redirect('/login');
            else
                return res.clearCookie('token').status(401).json( {message: 'User not found'} );
        }
        
    } catch (error) {
        //Token expired or jwt error
        if(req.serveBrowser)
            return res.clearCookie('token').redirect('/login');
        else
            return res.clearCookie('token').status(401).json( {message: 'Token verification failed', error: error} );
    }
}

// Verifies token and valid user
async function verifyNoToken(req, res, next){
    if(req.cookies.token === undefined)
        next();
    else
        res.redirect('/');

}

module.exports = { setToken, verifyToken, verifyNoToken }