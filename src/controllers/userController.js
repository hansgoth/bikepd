const database      = require('../database/db');
const bcrypt        = require('bcryptjs');
const jwt           = require('../middleware/jwt');
const roles         = require('../middleware/roles');
const { Validator } = require('node-input-validator');

const salt = bcrypt.genSaltSync(10); 


async function registerUser(req, res, next) {

    try{

        // Validate Inputs
        const validator = new Validator(req.body, {
            name: 'required',
            email: 'required|email',
            password: 'required|minLength:8',
            dni: 'required|minLength:8|alphaNumeric'
        });
        if (!await validator.check()) return res.status(400).json({message: 'Invalid inputs', error: validator.errors});

        let {name, email, password, dni} = req.body;
        password = bcrypt.hashSync(password, salt);

        // Queries
        await database.query('BEGIN');
        let query   = await database.query(`INSERT INTO public.users (name, email, password, dni) VALUES ('${name}', '${email}', '${password}', '${dni}') RETURNING *;`);
        let queryUR = await database.query(`INSERT INTO users_roles (user_id, role_id)
                                                   (SELECT ${query.rows[0].id}, public.roles.id FROM public.roles WHERE public.roles.name = 'user') RETURNING *;`);
        await database.query('COMMIT');

        // Response
        jwt.setToken(query.rows[0].id, res);
        if(req.serveBrowser)
            res.redirect('/');
        else
            res.status(200).json(query.rows[0]);
    }
    catch( error ){
        await database.query('ROLLBACK');
        console.error(error);
        res.status(500).json({message: 'Failed to register', error: error});
    }
}

async function loginUser(req, res, next) {
    try{

        // Validate Inputs
        const validator = new Validator(req.body, {
            email: 'required|email',
            password: 'required'
        });
        if (!await validator.check()) return res.status(400).json({message: 'Invalid inputs', error: validator.errors});
        
        let {email, password} = req.body;

        // Queries
        let query = await database.query(`SELECT * from public.users WHERE deleted_at IS NULL AND email = '${email}';`);

        // Response
        if(query.rowCount==1 && bcrypt.compareSync(password, query.rows[0].password) ){
            jwt.setToken(query.rows[0].id, res);
            if(req.serveBrowser){
                   res.redirect('/');
            }
            else
                res.status(200).json(query.rows[0]);
        }
        else {
            res.status(401).json({message:'Wrong credentials'});
        }
    }
    catch( error ){
        console.error(error);
        res.status(500).json({message: 'Failed to process login', error: error});
    }
}

async function logOut(req, res, next) {
    try{

            if(req.serveBrowser)
                res.clearCookie('token').render('login');
            else
                res.clearCookie('token').status(200).json({message: 'Logged out'});

    }
    catch( error ){
        console.error(error);
        res.clearCookie('token').status(500).json({message: 'Failed to process logout', error: error});
    }
}


module.exports = { registerUser, loginUser, logOut }