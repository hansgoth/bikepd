const database   = require('../database/db');
const { Validator } = require('node-input-validator');


async function searchBikes(req, res, next) {

    try{

        const validator = new Validator(req.body, {
            color: 'alphaDash',
            model: 'alphaNumeric',
            brand: 'alphaNumeric',
            status: 'alpha'
        });
        if (!await validator.check()) return res.status(400).json({message: 'Invalid inputs', error: validator.errors});
        let {license, color, model, brand, date, address, description, owner, status} = req.body;

        //Build search criterias
        let criteria = '';
        if(license != undefined && license != '') criteria += ` AND public.bikes.license ILIKE '%${license}%'`;
        if(color != undefined && color != '') criteria += ` AND public.bikes.color ILIKE '%${color}%'`;
        if(model != undefined && model != '') criteria += ` AND public.bikes.model ILIKE '%${model}%'`;
        if(brand != undefined && brand != '') criteria += ` AND public.bikes.brand ILIKE '%${brand}%'`;
        if(date != undefined && date != '') criteria += ` AND to_char(public.reports.theft_date, 'YYYY-MM-DD') LIKE '%${date}%'`;
        if(address != undefined && address != '') criteria += ` AND public.reports.theft_address ILIKE '%${address}%'`;
        if(description != undefined && description != '') criteria += ` AND public.reports.theft_description ILIKE '%${description}%'`;
        if(status != undefined && status != '') criteria += ` AND public.reports.report_status = '${status.toUpperCase()}'`;
        if(owner != undefined && owner != '') criteria += ` AND public.users.name ILIKE '%${owner}%'`;

        // Queries
        //await database.query('BEGIN');
        let query   = await database.query(`SELECT public.bikes.license, public.bikes.color, public.bikes.model, public.bikes.brand, 
                                                   public.users.name, public.users.email, public.users.dni,
                                                   public.reports.*, 
                                                   public.officers.badge, officer_u.name AS off_name,
                                                   public.departments.name AS dep_name, public.departments.address AS dep_address 
                                            FROM public.bikes 
                                            INNER JOIN public.users ON public.bikes.owner_id = public.users.id 
                                            INNER JOIN public.reports ON public.bikes.id = public.reports.bike_id 
                                            LEFT JOIN public.officers ON public.reports.officer_id = public.officers.id 
                                            LEFT JOIN public.users officer_u ON public.officers.user_id = officer_u.id
                                            LEFT JOIN public.departments ON public.officers.department_id = public.departments.id 
                                            WHERE public.bikes.deleted_at IS NULL ${criteria};`);
        //await database.query('COMMIT');

        // Response
        if(req.serveBrowser)
            res.render('reports', { reports: query.rows});
        else
            res.status(200).json(query.rows);
    }
    catch( error ){
        console.error(error);
        res.status(500).json({message: 'Failed search', error: error});
    }
}


// Auxilair function : distance from 2 coordinates
function distance(lat1,lng1, lat2,lng2){

    const R = 6371e3; //in metres
    let alfa1 = lat1 * Math.PI/180; //in radians
    let alfa2 = lat2 * Math.PI/180; //in radias
    let deltaLat = (lat2-lat1) * Math.PI/180;
    let deltaLng = (lng2-lng1) * Math.PI/180;

    let a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) + Math.cos(alfa1) * Math.cos(alfa2) * Math.sin(deltaLng/2) * Math.sin(deltaLng/2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    let distance = R * c; // in metres
    return distance;
}

module.exports = { searchBikes }