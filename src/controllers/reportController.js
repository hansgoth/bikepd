const database       = require('../database/db');
const { Validator }  = require('node-input-validator');
const axios          = require('axios');
const mailController = require('./mailController');

const REPORT_NEW      = 'NEW';
const REPORT_OPEN     = 'OPEN';
const REPORT_RESOLVED = 'RESOLVED';


async function createReport(req, res, next) {

    try{

        // Validate Inputs
        const validator = new Validator(req.body, {
            license: 'required',
            color: 'alphaDash',
            model: 'alphaNumeric',
            brand: 'alphaNumeric',
            date: 'required|date', // YYYY-MM-DD
            address: 'required',
            description: 'required'
        });
        if (!await validator.check()) return res.status(400).json({message: 'Invalid inputs', error: validator.errors});

        let {license, color, model, brand, date, address, description} = req.body;
        let {lat, lng} = await getGeoCoordinates(address);
        // Queries
        await database.query('BEGIN');
        let queryB = await database.query(`INSERT INTO public.bikes (license, color, model, brand, owner_id) VALUES('${license.toUpperCase()}', '${color.toLowerCase()}', '${model.toLowerCase()}', '${brand.toLowerCase()}', ${req.user.id}) RETURNING *;`);
        let queryR = await database.query(`INSERT INTO public.reports (user_id, bike_id, theft_date, theft_address, theft_description, lat, lng, report_status) VALUES( ${req.user.id}, ${queryB.rows[0].id}, '${date}', '${address}', '${description}', ${lat}, ${lng}, '${REPORT_NEW}') RETURNING *;`);
        await database.query('COMMIT');

        // Response
        if(req.serveBrowser){
            //res.render('report', queryR.rows[0]);
            req.params = {id: queryR.rows[0].id};
            getReport(req,res,next);
        }
        else
            res.status(200).json(queryR.rows[0]);
    }
    catch( error ){
        await database.query('ROLLBACK');
        console.error(error);
        res.status(500).json({message: 'Failed to register', error: error});
    }
}


async function getReports(req, res, next) {

    try{

        //TODO query params for specifi type of reports
        let status = req.query.status;
        let queryStatus = '';
        if(status != undefined && status != ''){
            queryStatus = ` AND public.reports.report_status='${status.toUpperCase()}'`;
        }

        // Queries
        let query = await database.query(`SELECT * FROM public.reports INNER JOIN public.bikes ON public.reports.bike_id = public.bikes.id WHERE public.reports.deleted_at IS NULL ${queryStatus};`);
        
        // Response
        res.status(200).json(query.rows);
    }
    catch( error ){
        console.error(error);
        res.status(500).json({message: 'Failed to feth reports', error: error});
    }
}


async function getReport(req, res, next) {

    try{

        let reportId = req.params.id | req.body.id;
        // Queries
        let query = await database.query(`SELECT public.reports.*, public.officers.badge, public.officers.user_id AS officer_user_id,
                                                 TO_CHAR(public.reports.theft_date, 'yyyy-mm-dd') AS theft_date,
                                                 public.bikes.license, public.bikes.color, public.bikes.model, public.bikes.brand 
                                            FROM public.reports 
                                            INNER JOIN public.bikes ON public.reports.bike_id = public.bikes.id 
                                            LEFT JOIN public.officers ON public.reports.officer_id = public.officers.id
                                            WHERE public.reports.deleted_at IS NULL AND public.reports.id=${reportId};`);
        
        // Validates report ownership. Current user must be reporter of officer in charge
        if( req.user.id != query.rows[0].user_id && req.user.id != query.rows[0].officer_user_id) {
            query.rows.pop();
        }

        
        // Response
        if(req.serveBrowser)
            res.render('report', { report: query.rows[0] });
        else
            res.status(200).json(query.rows[0]);

    }
    catch( error ){
        console.error(error);
        res.status(500).json({message: 'Failed to fetch report', error: error});
    }
}


async function closeReport(req, res, next) {

    try{

        // Validate current Officer vs report Officer
        let report = await database.query(`SELECT public.reports.*, public.users.email FROM public.reports 
                                           INNER JOIN public.users ON public.reports.user_id = public.users.id 
                                           WHERE public.reports.deleted_at IS NULL AND public.reports.id = ${req.params.id};`);
        
        if(report.rowCount==1 && report.rows[0].officer_id == req.officer.id && report.rows[0].report_status==REPORT_OPEN){
            
            // Queries
            await database.query('BEGIN');
            let email = report.rows[0].email; //Save email before loosing it on the update RETURNINg *
            report = await database.query(`UPDATE public.reports SET report_status='${REPORT_RESOLVED}' WHERE id=${req.params.id} RETURNING *;`);
            await database.query(`UPDATE public.officers SET idle='now()' WHERE id=${report.rows[0].officer_id}`);
            await database.query('COMMIT');

            //All good! send mail!
            let html = `<p>Bike Police Department</p>
                        <p>Report ID: ${report.rows[0].id} was resolved!</p>`;
            mailController.sendMail(process.env.SENDGRID_FROM, email, `BikePD - Report[${report.rows[0].id}] Resolved!`, html);

            // Response
            if(req.serveBrowser)
                res.redirect('/');
            else
                res.status(200).json(report.rows[0]);
        }
        else{
            res.status(403).json({message: 'Forbiden report'});
        }

    }
    catch( error ){
        await database.query('ROLLBACK');
        console.error(error);
        res.status(500).json({message: 'Failed to close report', error: error});
    }
}


// Auxiliar funtion to retrieve Google GeoCoordinates service
async function getGeoCoordinates(address) {
    
    let lat = 0;
    let lng = 0;
    try {
        
        //Google service
        let response = await axios.get(process.env.GOOGLE_GEO_SERVICE,{
            params:{
                address: address,
                key: process.env.GOOGLE_API_KEY
            }
        });

        if( response.data.status == 'OK') {
            //if found, set location
            lat = response.data.results[0].geometry.location.lat;
            lng = response.data.results[0].geometry.location.lng;
            console.log( `Geolocation[ latitude:${lat} - longitude:${lng}]`);
        }

    } catch (error) {        
        console.error( error );
    } finally {

        return {lat: lat, lng: lng};

    }



}


// Auxiliar function for Cron Job: Assigns new reports to idle officers
async function assignReports(req, res, next) {

    try {
        
        console.log(`[${new Date().toLocaleString()}] Running Job: Checking for new unassigned reports...`);
    
        // Query NEW reports (oldest first)
        let reports = await database.query(`SELECT public.reports.id, public.users.email FROM public.reports INNER JOIN public.users ON public.reports.user_id = public.users.id WHERE public.reports.deleted_at IS NULL AND public.reports.report_status='${REPORT_NEW}' ORDER BY public.reports.created_at ASC;`);
        
        if(reports.rowCount > 0){ // No reports, dont bother to look for officers

            //Query idle officers (longer idleness first)
            let officers = await database.query(`SELECT id FROM public.officers WHERE public.officers.deleted_at IS NULL AND public.officers.idle IS NOT NULL ORDER BY public.officers.idle ASC;`);
            for(let index=0; index<reports.rows.length; index++){ // No foreach, so I can break
                let report = reports.rows[index];
                if( officers.rows.length == 0) { // No more officers
                    console.log(`  No Police Officers available - Remaining reports [${reports.rows.length-index}]`);
                    break;
                }

                // Assignments: Pop first officer, assign oldest report
                let officer = officers.rows.shift();//splice(0,1)[0];
                await database.query(`UPDATE public.reports SET officer_id=${officer.id}, report_status='${REPORT_OPEN}' WHERE id=${report.id};`);
                await database.query(`UPDATE public.officers SET idle=NULL WHERE id=${officer.id};`);
                console.log(`  Report[${report.id}] assigend to Officer[${officer.id}]`);

                //Report status changed! send mail
                let html = `<p>Bike Police Department</p>
                            <p>Investigation on Report ID: ${report.id} has started!</p>`;
                mailController.sendMail(process.env.SENDGRID_FROM, report.email, `BikePD - Report[${report.id}] Status update!`, html);
            }
        }
        else console.log('  No new reports... chillout!');

    } catch (error) {
        console.error(error);
    }

}


module.exports = { REPORT_NEW,REPORT_OPEN,REPORT_RESOLVED, createReport, getReports, getReport, closeReport, assignReports }