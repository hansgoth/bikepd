const email = require('@sendgrid/mail');
const { Validator } = require('node-input-validator');

//Sendgrip 
async function sendMail(from, to, subject, html) {

    try {

        const validator = new Validator( {from: from, to: to}, {
            from: 'required|email',
            to: 'required|email'
        });
        if (!await validator.check()){
            console.error(validator.errors);
            return;
        }
        
        email.setApiKey(process.env.SENDGRID_API_KEY);

        const msg = {
            to: to,
            from: from,
            subject: subject,
            text: 'text',
            html: html
        }

        email.send(msg)
            .then(() => {
                console.log(`Email sent [${from}] -> [${to}]`);
            })
            .catch((error) => {
                console.error(error);
            })

    }
    catch (error) {
        console.error(error);
    }
}

module.exports = { sendMail }

