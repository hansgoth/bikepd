const database      = require('../database/db');
const { Validator } = require('node-input-validator');
const roles         = require('../middleware/roles');
const reportController = require('./reportController');

async function showLogin(req, res, next){
    res.render('login');
};


async function showRegister(req, res, next){
    res.render('register');
};


async function showDashboard(req, res, next){
    
    if(await roles.isRole(req.user, roles.ROLE_SUPERADMIN)){
        let departments = await getDepartments();
        res.render('index_admin', { user: req.user, departments: departments});
    }
    else if(await roles.isRole(req.user, roles.ROLE_DIRECTOR)){
        await roles.getOfficer(req); //Director Officer
        let officers = await getDepartmentOfficers(req.officer.department_id, req.user.id); //Other Officers
        res.render('index_dir', { user: req.user, officer: req.officer, officers: officers });
    }
    else if( await roles.isRole(req.user, roles.ROLE_OFFICER)){
        await roles.getOfficer(req);
        let report = await getOpenReport(req.officer.id);
        res.render('index_officer', { user: req.user, officer: req.officer, report: report });
    }
    else{
        res.render('index', { user: req.user });
    }

}


async function getFavicon(req, res, next){
    res.status(200).send();
};



// Auxiliar functions

async function getOpenReport(officerId){
    try {
        let query = await database.query(`SELECT public.reports.*, TO_CHAR(public.reports.theft_date, 'yyyy-mm-dd') AS theft_date,
                                                 public.bikes.license, public.bikes.color, public.bikes.model, public.bikes.brand 
                                            FROM public.reports 
                                            INNER JOIN public.bikes ON public.reports.bike_id = public.bikes.id 
                                            WHERE public.reports.deleted_at IS NULL AND public.reports.report_status='${reportController.REPORT_OPEN}' AND public.reports.officer_id=${officerId};`);
        return query.rows[0];
    } catch (error) {
        return undefined;
    }
}

async function getDepartmentOfficers(depId, dirUserId){
    try {
        let query = await database.query(`SELECT public.officers.*, public.users.name, public.users.email FROM public.officers 
                                            INNER JOIN public.users ON public.officers.user_id = public.users.id
                                            WHERE public.officers.deleted_at IS NULL AND public.officers.department_id = ${depId} AND public.officers.user_id != ${dirUserId};`);
        return query.rows;
    } catch (error) {
        return [];
    }
}



async function getDepartments(){
    try {
        let query = await database.query(`SELECT * FROM public.departments WHERE deleted_at IS NULL`);
        return query.rows;
    } catch (error) {
        return [];
    }
}

module.exports = { showLogin, showRegister, showDashboard, getFavicon }