const database   = require('../database/db');
const bcrypt        = require('bcryptjs');
const { Validator } = require('node-input-validator');

const salt = bcrypt.genSaltSync(10); 

async function addOfficer(req, res, next) {

    try{
        //TODO
        // Validate Inputs
        const validator = new Validator(req.body, {
            name: 'required',
            email: 'required|email',
            password: 'required|minLength:8',
            dni: 'required|minLength:8|alphaNumeric',
            badge: 'required'
        });
        if (!await validator.check()) return res.status(400).json({message: 'Invalid inputs', error: validator.errors});
        let { name, email, password, dni, badge } = req.body;
        password = bcrypt.hashSync(password, salt);


        // Queries
        await database.query('BEGIN');
        let queryU  = await database.query(`INSERT INTO public.users (name, email, password, dni) VALUES ('${name}', '${email}', '${password}', '${dni}') RETURNING *;`);
        let queryUR = await database.query(`INSERT INTO users_roles (user_id, role_id)
                                                (SELECT ${queryU.rows[0].id}, public.roles.id FROM public.roles WHERE public.roles.name = 'officer') RETURNING *;`);
        let queryO   = await database.query(`INSERT INTO public.officers (user_id, department_id, badge, idle) VALUES(${queryU.rows[0].id}, ${req.officer.department_id}, ${badge}, now()) RETURNING *;`);
        await database.query('COMMIT');

        // Response
        if(req.serveBrowser)
            res.redirect('/');
        else
            res.status(200).json(queryO.rows[0]);

    }
    catch( error ){
        await database.query('ROLLBACK');
        console.error(error);
        res.status(500).json({message: 'Failed to add Officer', error: error});
    }
}


module.exports = { addOfficer }