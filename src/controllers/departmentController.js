const database   = require('../database/db');
const bcrypt        = require('bcryptjs');
const { Validator } = require('node-input-validator');

const salt = bcrypt.genSaltSync(10); 

async function addDepartment(req, res, next) {

    try{
        //TODO
        // Validate Inputs
        const validator = new Validator(req.body, {
            dep_name: 'required',
            dep_address: 'required',
            name: 'required',
            email: 'required|email',
            password: 'required|minLength:8',
            dni: 'required|minLength:8|alphaNumeric',
            badge: 'required'
        });
        if (!await validator.check()) return res.status(400).json({message: 'Invalid inputs', error: validator.errors});
        let { dep_name, dep_address, name, email, password, dni, badge } = req.body;
        password = bcrypt.hashSync(password, salt);

        // Queries
        await database.query('BEGIN');
        let queryD  = await database.query(`INSERT INTO public.departments (name, address, lat, lng) VALUES ('${dep_name}', '${dep_address}', 0, 0) RETURNING *;`)
        let queryU  = await database.query(`INSERT INTO public.users (name, email, password, dni) VALUES ('${name}', '${email}', '${password}', '${dni}') RETURNING *;`);
        let queryUR = await database.query(`INSERT INTO users_roles (user_id, role_id)
                                                (SELECT ${queryU.rows[0].id}, public.roles.id FROM public.roles WHERE public.roles.name = 'director') RETURNING *;`);
        let queryO   = await database.query(`INSERT INTO public.officers (user_id, department_id, badge ) VALUES(${queryU.rows[0].id}, ${queryD.rows[0].id}, ${badge}) RETURNING *;`);
        await database.query('COMMIT');

        // Response
        if(req.serveBrowser)
            res.redirect('/');
        else
            res.status(200).json(queryO.rows[0]);

    }
    catch( error ){
        await database.query('ROLLBACK');
        console.error(error);
        res.status(500).json({message: 'Failed to add Department', error: error});
    }
}


module.exports = { addDepartment }