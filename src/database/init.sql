-- Init DB
--CREATE DATABASE bikepd_db


-- Clean
DROP TABLE IF EXISTS public.reports;
DROP TABLE IF EXISTS public.officers;
DROP TABLE IF EXISTS public.bikes;
DROP TABLE IF EXISTS public.departments;
DROP TABLE IF EXISTS public.users_roles;
DROP TABLE IF EXISTS public.users;
DROP TABLE IF EXISTS public.roles;


-- Users Table
CREATE TABLE public.users (
	id serial PRIMARY KEY,
	name VARCHAR(100) NOT NULL,
	email VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    dni VARCHAR(10) NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL
);
INSERT INTO public.users (name, email, password, dni) VALUES ('SuperAdmin', 'superadmin@bikepd.com', '$2a$10$ioVDu9xtAJSdGWansPYQfONxhjw6gD7ZTtJ2jE1607mJ3qM3/eBJ2', 0);

-- Roles
CREATE TABLE public.roles (
	id serial PRIMARY KEY,
	name VARCHAR(25) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL
);
INSERT INTO public.roles (name) VALUES ('user'),('officer'),('director'),('superadmin');

-- Users Roles
CREATE TABLE public.users_roles (
	user_id INTEGER REFERENCES public.users (id),
    role_id INTEGER REFERENCES public.roles (id),
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL,
    PRIMARY KEY (user_id, role_id)
);
INSERT INTO public.users_roles (user_id,role_id) VALUES (1,4);

-- Departments
CREATE TABLE public.departments (
	id serial PRIMARY KEY,
    name VARCHAR(50) NOT NULL UNIQUE,
    address VARCHAR(100) NOT NULL,
    lat NUMERIC(16,13) NULL,
    lng NUMERIC(16,13) NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL
);

-- Officers
CREATE TABLE public.officers (
	id serial PRIMARY KEY,
    user_id INTEGER REFERENCES public.users (id),
    department_id INTEGER REFERENCES public.departments (id),
    badge INTEGER NOT NULL UNIQUE,
    idle TIMESTAMP NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL
);

-- Bikes
CREATE TABLE public.bikes (
	id serial PRIMARY KEY,
    license VARCHAR(10) NOT NULL UNIQUE,
    color VARCHAR(50) NULL,
    model VARCHAR(100) NULL,
    brand VARCHAR(100) NULL,
    owner_id INTEGER REFERENCES public.users (id),
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL
);

-- Reports
CREATE TABLE public.reports (
	id serial PRIMARY KEY,
	user_id INTEGER REFERENCES public.users (id),
    officer_id INTEGER NULL REFERENCES public.officers (id),
    bike_id INTEGER REFERENCES public.bikes (id),
    theft_date DATE NOT NULL,
    theft_address VARCHAR(100) NOT NULL,
    theft_description TEXT NOT NULL,
    lat NUMERIC(16,13) NULL,
    lng NUMERIC(16,13) NULL,
    report_status VARCHAR(10) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL
);




-- Test : initial load

-- department
INSERT INTO public.departments ("name", address) VALUES('Seccional 19', 'Carlos Mauricio Balndy 44, Las Palamas de Gran Canaria, España');

-- police director
INSERT INTO public.users (name, email, password, dni) VALUES ('Capitan Nemo', 'nemo@bikepd.com', '$2a$10$ioVDu9xtAJSdGWansPYQfONxhjw6gD7ZTtJ2jE1607mJ3qM3/eBJ2', '11234569L');
INSERT INTO public.officers (user_id, department_id, badge ) VALUES(2, 1, 15245);
INSERT INTO public.users_roles (user_id,role_id) VALUES (2,1);
INSERT INTO public.users_roles (user_id,role_id) VALUES (2,2);
INSERT INTO public.users_roles (user_id,role_id) VALUES (2,3);

-- police officer
INSERT INTO public.users (name, email, password, dni) VALUES ('Cabo Cañaveral', 'canaveral@bikepd.com', '$2a$10$ioVDu9xtAJSdGWansPYQfONxhjw6gD7ZTtJ2jE1607mJ3qM3/eBJ2', '22354697F');
INSERT INTO public.officers (user_id, department_id, badge, idle ) VALUES(3, 1, 78147, now());
INSERT INTO public.users_roles (user_id,role_id) VALUES (3,1);
INSERT INTO public.users_roles (user_id,role_id) VALUES (3,2);

INSERT INTO public.users (name, email, password, dni) VALUES ('Cabo Verde', 'verde@bikepd.com', '$2a$10$ioVDu9xtAJSdGWansPYQfONxhjw6gD7ZTtJ2jE1607mJ3qM3/eBJ2', '45434567K');
INSERT INTO public.officers (user_id, department_id, badge, idle ) VALUES(4, 1, 78999, now());
INSERT INTO public.users_roles (user_id,role_id) VALUES (4,1);
INSERT INTO public.users_roles (user_id,role_id) VALUES (4,2);

-- Reports
--INSERT INTO public.users ("name",email,"password",dni) VALUES ('Ariel Sanchez','hansgoth@hotmail.com','$2a$10$ioVDu9xtAJSdGWansPYQfONxhjw6gD7ZTtJ2jE1607mJ3qM3/eBJ2','16821629G');
--INSERT INTO public.users_roles (user_id, role_id) VALUES(5, 1);
--INSERT INTO public.bikes (license,color,model,brand,owner_id) VALUES ('XXX-123','red','bmx','superbikes',5), ('PT-001','blue','urban01','cycletas',5);
--INSERT INTO public.reports (user_id,officer_id,bike_id,theft_date,theft_address,theft_description,lat,lng,report_status) VALUES (5,NULL,1,'2021-01-15','Carlos Mauricio Blandy 44, Las Plamas de Gran Canarias, España','Chorrito zorete punga ladri rata',0,0,'NEW'), (5,NULL,2,'1973-03-15','Carlos Mauricio Blandy 44, Las Plamas de Gran Canarias, España','Ya ni me acuerdo que paso',0,0,'NEW');

-- Assign Report
--UPDATE public.reports SET officer_id=2, report_status='OPEN' WHERE id=1;

-- Close Report
--UPDATE public.reports SET report_status='RESOLVED' WHERE id=1;
